<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkeyToKomentarPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('komentar_pertanyaan', function (Blueprint $table) {
            $table->unsignedBigInteger('id_pertanyaan');
            $table->unsignedBigInteger('id_profile');
            $table->foreign('id_profile')->references('id_profile')->on('profile');
            $table->foreign('id_pertanyaan')->references('id_pertanyaan')->on('pertanyaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentar_pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['id_profile', 'id_pertanyaan']);
            $table->dropColumn(['id_profile', 'id_pertanyaan']);
        });
    }
}
