<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkeyToPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->unsignedBigInteger('id_profile');
            $table->unsignedBigInteger('id_jawaban');
            $table->foreign('id_profile')->references('id_profile')->on('profile');
            $table->foreign('id_jawaban')->references('id_jawaban')->on('jawaban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['id_profile', 'id_jawaban']);
            $table->dropColumn(['id_profile', 'id_jawaban']);
        });
    }
}
